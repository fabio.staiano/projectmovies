//
//  SuggestionsView.swift
//  Project Movies
//
//  Created by Fabio Staiano on 07/02/2020.
//  Copyright © 2020 Fabio Staiano. All rights reserved.
//

import UIKit

class SuggestionsView: UIViewController, UITableViewDataSource, UITableViewDelegate {
        
    @IBOutlet weak var suggestionTable: UITableView!
    
    let cellReuseIdentifier = "SuggestionCell"
    var movies = [Movies]()
    let defaults = UserDefaults.standard
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.suggestionTable.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! SuggestionCell
        
        cell.suggestionTitle?.text = movies[indexPath.row].name
        cell.suggestionSub?.text = movies[indexPath.row].genre
        cell.suggestionNumber?.text = "☆"
        
        
        cell.layer.shadowOffset = CGSize(width: 3, height: 3)
        cell.layer.shadowRadius = 7.0
        cell.layer.shadowColor = UIColor.darkGray.cgColor
        
        let url = URL(string: movies[indexPath.row].wideImageUrl)
        let data = try? Data(contentsOf: url!)
        cell.suggestionImage.setImage(UIImage(data: data!), for: .normal)
        cell.suggestionImage.imageView?.contentMode = .scaleAspectFill
        cell.suggestionImage.layer.cornerRadius = 5
        cell.suggestionImage.layer.masksToBounds = true
        
        return cell
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.suggestionTable.separatorStyle = UITableViewCell.SeparatorStyle.none

        suggestionTable.delegate = self
        suggestionTable.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if suggestionTable.visibleCells.isEmpty {
            suggestionTable.alpha = 0
        } else {
            suggestionTable.alpha = 1
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        fetchMovies()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movies.count
    }
    
    func fetchMovies () {
        
        let userID = defaults.object(forKey: "UserID") as! Int
        
        let url = URL(string: "https://ssprojectmovies.herokuapp.com/getrecommended/" + String(userID) )!
        
        URLSession.shared.dataTask(with: url){data, responder, error
            in
            guard let data = data else {
                print(error?.localizedDescription ?? "Unknown Error")
                return
            }
            
            let decoder = JSONDecoder()
            
            if let movies = try? decoder.decode([Movies].self, from: data){
                DispatchQueue.main.async {
                    self.movies = movies
                    self.suggestionTable.reloadData()
                    self.suggestionTable.alpha = 1
                    print("Loaded \(movies.count) movies")
                }
            } else {
                print("Unable to parse JSON")
            }
        }.resume()
    }
    
    
}
