//
//  SuggestionCell.swift
//  Project Movies
//
//  Created by Fabio Staiano on 07/02/2020.
//  Copyright © 2020 Fabio Staiano. All rights reserved.
//

import UIKit

class SuggestionCell: UITableViewCell {
    
    @IBOutlet weak var suggestionTitle: UILabel!
    @IBOutlet weak var suggestionSub: UILabel!
    @IBOutlet weak var suggestionNumber: UILabel!
    @IBOutlet var suggestionImage: RoundButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
