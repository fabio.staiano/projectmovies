//
//  SuggestionsView.swift
//  Project Movies
//
//  Created by Fabio Staiano on 07/02/2020.
//  Copyright © 2020 Fabio Staiano. All rights reserved.
//

import UIKit


class FavoritesView: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var suggestionTable: UITableView!
    
    let cellReuseIdentifier = "SuggestionCell"
    
    var movies : [Movies] = [Movies(suggestionTitle: "", suggestionSub: "", suggestionNumber: "1"), Movies(suggestionTitle: "", suggestionSub: "", suggestionNumber: "2"), Movies(suggestionTitle: "", suggestionSub: "", suggestionNumber: "3")]
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.suggestionTable.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! SuggestionCell


        return cell
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.suggestionTable.separatorStyle = UITableViewCell.SeparatorStyle.none

        suggestionTable.delegate = self
        suggestionTable.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.movies.count
    }
}
