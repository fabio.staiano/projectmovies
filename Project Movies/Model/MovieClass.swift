//
//  MovieClass.swift
//  Project Movies
//
//  Created by Fabio Staiano on 07/02/2020.
//  Copyright © 2020 Fabio Staiano. All rights reserved.
//

import Foundation

public struct Movies : Codable {
    var id: Int
    var name: String
    var genre: String
    var posterImageUrl: String
    var wideImageUrl: String
}

public func newMovies () {
    let page = 3
    
    let api = URL(string: "https://api.themoviedb.org/3/discover/movie?api_key=a074a415d455aa641cdfc5a76b9e2e5b&vote_count.gte=1000&primary_release_date.gte=2010-01-01&&page=" + String(page) + "&sort_by=vote_average.desc")!
    
    URLSession.shared.dataTask(with: api){data, responder, error
        in
        guard let data = data else {
            print(error?.localizedDescription ?? "Unknown Error")
            return
        }
        
        let decoder = JSONDecoder()
        
        let result = try? decoder.decode(Result.self, from: data)
        
        result!.results.forEach{ movie in
            
            let posterImageUrl = "https://image.tmdb.org/t/p/w500/" + movie.posterPath
            let wideImageUrl = "https://image.tmdb.org/t/p/w500/" + movie.backdropPath
            let genre = getGenre(genreID: movie.genreIDS[0])
            
            print(movie.title)
            print(genre)
            print(posterImageUrl)
            
            let url = String(format: "https://ssprojectmovies.herokuapp.com/movies")
            guard let serviceUrl = URL(string: url) else { return }
            
            
            let parameterDictionary = ["posterImageUrl": posterImageUrl,
                                       "wideImageUrl": wideImageUrl,
                                       "name": movie.title,
                                       "genre": genre]
            
            var request = URLRequest(url: serviceUrl)
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameterDictionary, options: []) else { return }
            request.httpBody = httpBody

            URLSession.shared.dataTask(with: request) { (data, response, error) in
                if let data = data {
                    do {
                        if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                            
                            print(json)
                            
                    }
                        
                    } catch {
                        print(error)
                    }
                }
                
            }.resume()
            
        }
    }.resume()
}

public func getGenre(genreID: Int) -> String {
    
    var genre : String?
    
    switch genreID {
    case 28:
        return "Action"
    case 12:
        return "Adventure"
    case 16:
        return "Animation"
    case 35:
        return "Comedy"
    case 80:
        return "Crime"
    case 99:
        return "Documentary"
    case 18:
        return "Drama"
    case 10751:
        return "Family"
    case 14:
        return "Fantasy"
    case 36:
        return "History"
    case 27:
        return "Horror"
    case 10402:
        return "Music"
    case 9648:
        return "Mystery"
    case 10749:
        return "Romance"
    case 878:
        return "Science Fiction"
    case 10770:
        return "TV Movie"
    case 53:
        return "Thriller"
    case 10752:
        return "War"
    case 37:
        return "Western"
    default:
        return ""
    }
}


// MARK: - Result
struct Result: Codable {
    let page, totalResults, totalPages: Int
    let results: [ResultElement]

    enum CodingKeys: String, CodingKey {
        case page
        case totalResults = "total_results"
        case totalPages = "total_pages"
        case results
    }
}

// MARK: - ResultElement
struct ResultElement: Codable {
    let popularity: Double
    let voteCount: Int
    let video: Bool
    let posterPath: String
    let id: Int
    let adult: Bool
    let backdropPath, originalLanguage, originalTitle: String
    let genreIDS: [Int]
    let title: String
    let voteAverage: Double
    let overview, releaseDate: String

    enum CodingKeys: String, CodingKey {
        case popularity
        case voteCount = "vote_count"
        case video
        case posterPath = "poster_path"
        case id, adult
        case backdropPath = "backdrop_path"
        case originalLanguage = "original_language"
        case originalTitle = "original_title"
        case genreIDS = "genre_ids"
        case title
        case voteAverage = "vote_average"
        case overview
        case releaseDate = "release_date"
    }
}
