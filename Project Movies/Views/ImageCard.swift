//
//  FirstViewController.swift
//  Project Movies
//
//  Created by Fabio Staiano on 28/01/2020.
//  Copyright © 2020 Fabio Staiano. All rights reserved.
//

import UIKit

public class ImageCard: CardView {

    init(frame: CGRect, title: String, genre: String, imageUrl: String) {
        
        super.init(frame: frame)
        
        // image
        let url = URL(string: imageUrl)
        let data = try? Data(contentsOf: url!)
        let imageView = UIImageView(image: UIImage(data: data!))
        imageView.contentMode = .scaleAspectFill
        imageView.backgroundColor = UIColor(red: 67/255, green: 79/255, blue: 182/255, alpha: 1.0)
        imageView.layer.cornerRadius = 5
        imageView.layer.masksToBounds = true
        
        imageView.frame = CGRect(x: 12, y: 12, width: self.frame.width - 24, height: self.frame.height - 103)
        self.addSubview(imageView)
        
        
        let textBox1 = UILabel()
        
        textBox1.text = title
        textBox1.textColor = .darkGray
        textBox1.font = UIFont.boldSystemFont(ofSize: 24.0)
        textBox1.frame = CGRect(x: 13, y: imageView.frame.maxY + 20, width: 330, height: 24)
        self.addSubview(textBox1)
        
//        let textBox1 = UIView()
//        textBox1.backgroundColor = UIColor(red: 183/255, green: 183/255, blue: 183/255, alpha: 1.0)
//        textBox1.layer.cornerRadius = 12
//        textBox1.layer.masksToBounds = true
//
//        textBox1.frame = CGRect(x: 12, y: imageView.frame.maxY + 15, width: 200, height: 24)
//        self.addSubview(textBox1)
        
        let textBox2 = UILabel()
        textBox2.text = genre
        textBox2.textColor = .gray
        textBox2.font = UIFont(name: "HelveticaNeue-Light",size: 15.0)
        textBox2.frame = CGRect(x: 13, y: textBox1.frame.maxY + 3, width: 120, height: 24)
        self.addSubview(textBox2)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
